# Projet de blog full JavaScript
## Les technos
- Back-end : NestJS
- Front-end : NextJS
- Base de données : MongoDB

## prérequis
- Un PC
- NodeJS
- MongoDB
- NPM
- Postman (facultatif)
    - URL répertoire post avec toutes ses requêtes : https://www.getpostman.com/collections/3a5abb524d8b9c71c592

## Répertoire Postman
Pour intégrer notre répertoire Postman, cliquez sur le bouton "import" en haut à gauche de l'interface.

Ensuite, cliquez sur "l'onglet" "Link". Collez l'URL ci-dessus et cliquez sur "continuer".

Une fois cela fait, une nouvelle collection nommée "NestJS backend (tp 2022)" s'ajoute à votre liste.

Vous pouvez dorénavant accéder à toutes les requêtes nécessaires.

## Coté Back End
### Initialisation
Aller sur le terminal. et ce rendre sur le répertoire "blog-js/back/"

    cd ./blog-js/back/

Installer les dépendances NodeJS nécessaire au projet :

    npm install

Lancer le serveur :

    npm run start

Actuellement, le serveur écoute sur l'adresse 'localhost' et sur le port '3000'.

URL exemple : http://localhost:3000

Pour changer le port, rendez-vous dans le dossier back :

    cd ./blog-js/back/

Et modifiez la valeur de la première ligne *APP_PORT* du fichier .env

Toutes les routes sont présentes dans Postman.

Enjoy !