import {data} from "../api/register";
import Post_blog from "../components/post";
import Nav from "../components/nav";
import Footer from "../components/footer";
import ButtonPrevious from "../components/previousButton";
import ButtonNext from "../components/buttonNext";

export default function Post ({ post, date, page}) {

    return (
        <>
            <div className="flex flex-col h-screen">
                <div className="flex flex-row h-screen">
                    <Nav/>
                    <div className=" relative max-w-7xl mx-auto bg-gray-200 m-10 pt-10 pb-10 shadow-2xl">
                    <Post_blog
                        id={post.id}
                        title={post.title}
                        body={post.body}
                        date = {date}
                        page = {page}
                    />
                        <div className="absolute bottom-5 left-5 mt-20">
                            <ButtonPrevious page = {page - 1} />
                        </div>
                        <div className="absolute bottom-5 right-5 mt-20">
                            <ButtonNext page = {page + 1} />
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        </>
    )
}

export async function getStaticProps({params}) {
    const post = await fetch(`https://jsonplaceholder.typicode.com/posts/${params.id}`)
        .then(r => r.json())

    return {
        props: {
            post,
            date: (new Date()).toLocaleDateString(),
            page: (parseInt(params.id)),
        }
    }
}

export async function getStaticPaths() {
    return {

            paths : data.map(post => ({
                params: {id: post.id.toString()}
            })),
            fallback: false,
        }
}