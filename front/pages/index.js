import React, { useEffect, useState } from "react";
import {data} from "./api/register";
import {categories} from "./api/categories";
import Router, {useRouter} from "next/router";

export default function Index({}) {

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [message, setMessage] = useState('vous n\'êtes pas connecté')
    const [secret, setSecret] = useState('secret')
    const router = useRouter();

    useEffect(() => {
        localStorage.removeItem('token');
    }, [])

    async function submitForm(){
        const res = await fetch('api/login', {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({username, password })
            }).then((t) => t.json())

        const token = res.token

        if (token){

            const res = await fetch('api/secret', {
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/json'
                },
                body: JSON.stringify({token })
            }).then((t) => t.json())

            if (res.secretAdminCode){
                setSecret(res.secretAdminCode)
                localStorage.setItem('token', res.secretAdminCode)
                await router.push('/home')

            } else {
                setSecret('pas correct')
            }
        } else {
            setMessage('Une erreur c\'est produite')
        }

    }

    return (
        <>
            <h1>{message}</h1>
            <h1>{secret}</h1>
            <form>
                <input type="text" name="username" value={username} onChange={(e) => setUsername(e.target.value)}/>
                <input type="password" name="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                <input type="button" value="login" onClick={submitForm}/>
            </form>
        </>
    )
}

export async function getStaticProps() {

    return {
        props: {
            categories,
            data,
        }
    }
}