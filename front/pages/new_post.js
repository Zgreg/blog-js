import Nav from "./components/nav";
import Footer from "./components/footer";
import {useState} from "react";
import Layout from "../layouts/layout";


export default function Form() {
    const [name, setName] = useState('');
    const [prenom, setPrenom] = useState('');
    const [titre, setTitre] = useState('');
    const [article, setArticle] = useState('');

    const handleSubmit = e => {
        e.preventDefault();
        const data = {
            name,
            prenom,
            titre,
            article,
        };
        console.log(data);
    };

    return (
            <Layout>
            <div className="flex flex-col h-screen">
                <div className="flex flex-row h-screen">
                <Nav/>
                <div className="flex flex-row mt-10 ml-20">
                    <h3 className="text-3xl font-bold"> Publier un article</h3>
                    <div className="flex justify-center">
                    <form onSubmit={handleSubmit} className="relative flex flex-col h-96 justify-center max-w-2xl">
                            <div className="flex flex-wrap -mx-3 mb-6">
                                <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                                           htmlFor="nom">
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                                        id="nom" type="text" placeholder="Nom" onChange={e => setName({ ...name, content: e.target.value })}>
                                    </input>
                                </div>
                                <div className="w-full md:w-1/2 px-3">
                                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                                           htmlFor="prenom">
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        id="prenom" type="text" placeholder="Prénom" onChange={e => setPrenom({ ...prenom, content: e.target.value })}>
                                    </input>
                                </div>
                            </div>
                        <div>
                            <input
                            className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                            id="titre" type="text" placeholder="Titre" onChange={e => setTitre({ ...titre, content: e.target.value })}>
                            </input>
                        </div>
                        <div>
                            <textarea
                                className="mt-10 appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                id="article" placeholder="Article" onChange={e => setArticle({ ...article, content: e.target.value })}>
                            </textarea>
                        </div>
                        <button type="submit"
                            className="absolute bottom-0 right-0 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded lg:text-right">
                            Button
                        </button>
                        </form>
                    </div>
                </div>
                </div>
                <Footer/>
            </div>
            </Layout>
    )
}