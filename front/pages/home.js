import Head from 'next/head'
import Link from 'next/link'
import Nav from "./components/nav";
import Footer from "./components/footer";
import {data} from "./api/register";
import {categories} from "./api/categories";
import Layout from "../layouts/layout";

export default function Home({}) {

    return (
        <Layout>
            <Head>
                <title>Blog MyDigitalSchool</title>
            </Head>
            <div className="flex flex-col h-screen">
                <div className="flex flex-row h-screen">
                    <Nav/>
                    <div className=" relative max-w-7xl mx-auto bg-gray-200 m-10 p-10 pb-10 shadow-2xl">
                        <h1 className="text-3xl font-bold underline">
                            Mes publications :
                        </h1><br/>
                        <ul>
                            {data.map(data =>
                                <li>
                                    <Link href={`/blog_post/${data.id}`}>
                                        <a>
                                            <h3>Publication : {data.id} - {data.title}</h3>
                                        </a>
                                    </Link>
                                </li>)}
                        </ul>
                    </div>
                </div>
                <Footer/>
            </div>
        </Layout>
    )
}

export async function getStaticProps() {

    return {
        props: {
            categories,
            data,
        }
    }
}