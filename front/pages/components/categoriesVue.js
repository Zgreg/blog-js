import {categories} from "../api/categories";
import Link from "next/link";

export default function CatVue() {
    return (
        <>
            {categories.map(category =>
                <li className="p-5 pb-1">
                    <Link href={`/category/${category.id}`}>
                        <a>
                            {category.name}
                        </a>
                    </Link>
                </li>
            )}
        </>
    )
}