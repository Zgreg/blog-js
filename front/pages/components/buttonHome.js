import Link from "next/link";

export default function button() {
    return (
        <strong className="h-48">
            <Link href={'/'}>
                <button
                    className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
                    <a>
                        Revenir à l'Accueil
                    </a>
                </button>
            </Link>
        </strong>
    )
}