import Link from "next/link";
import CatVue from "./categoriesVue";
import Script from 'next/script'

export default function Nav() {
    return (
        <>
            <div className="px-4 border-r bg-white">
                <div className=" flex flex-col text-gray-500">
                    <h3 className="pl-1 text-sm flex items-center py-2 mb-2 hover:bg-gray-100 hover:text-gray-700 transition duration-200 ease-in">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-2" viewBox="0 0 20 20"
                             fill="black">
                            <path
                                d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"/>
                        </svg>
                        <a className="hover:text-black transition duration-200 ease-linear" href="/">Home</a>
                    </h3>
                    <div className="max-w-lg mx-auto">
                        <button
                            className="pl-1 text-sm flex items-center py-2 mb-2 hover:bg-gray-100 hover:text-gray-700 transition duration-200 ease-in"
                            type="button" data-dropdown-toggle="dropdown">
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-2" viewBox="0 0 20 20"
                                 fill="black">
                                <path
                                    d="M2 11a1 1 0 011-1h2a1 1 0 011 1v5a1 1 0 01-1 1H3a1 1 0 01-1-1v-5zM8 7a1 1 0 011-1h2a1 1 0 011 1v9a1 1 0 01-1 1H9a1 1 0 01-1-1V7zM14 4a1 1 0 011-1h2a1 1 0 011 1v12a1 1 0 01-1 1h-2a1 1 0 01-1-1V4z"/>
                            </svg>
                            Catégories
                            <svg className="w-4 h-4 ml-2"
                                                                                               fill="none"
                                                                                               stroke="currentColor"
                                                                                               viewBox="0 0 24 24"
                                                                                               xmlns="http://www.w3.org/2000/svg">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                  d="M19 9l-7 7-7-7">
                            </path>
                            </svg>
                        </button>
                        <div
                            className="hidden bg-white text-base z-50 list-none divide-y divide-gray-100 rounded shadow my-4"
                            id="dropdown">
                            <ul className="py-1" aria-labelledby="dropdown">
                                <CatVue/>
                            </ul>
                        </div>
                    </div>

                    <h3 className="pl-1 text-sm flex items-center py-2 mb-2 hover:bg-gray-100 hover:text-gray-700 transition duration-200 ease-in">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-2" viewBox="0 0 20 20"
                             fill="black">
                            <path fillRule="evenodd" d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
                                  clipRule="evenodd"/>
                        </svg>
                        <a className="hover:text-black transition duration-200 ease-linear" href="/new_post">Publier</a>
                    </h3>
                    <h3 className="pl-1 text-sm flex items-center py-2 mb-2 hover:bg-gray-100 hover:text-gray-700 transition duration-200 ease-in">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-2" viewBox="0 0 20 20"
                             fill="black">
                            <path fillRule="evenodd"
                                  d="M11.49 3.17c-.38-1.56-2.6-1.56-2.98 0a1.532 1.532 0 01-2.286.948c-1.372-.836-2.942.734-2.106 2.106.54.886.061 2.042-.947 2.287-1.561.379-1.561 2.6 0 2.978a1.532 1.532 0 01.947 2.287c-.836 1.372.734 2.942 2.106 2.106a1.532 1.532 0 012.287.947c.379 1.561 2.6 1.561 2.978 0a1.533 1.533 0 012.287-.947c1.372.836 2.942-.734 2.106-2.106a1.533 1.533 0 01.947-2.287c1.561-.379 1.561-2.6 0-2.978a1.532 1.532 0 01-.947-2.287c.836-1.372-.734-2.942-2.106-2.106a1.532 1.532 0 01-2.287-.947zM10 13a3 3 0 100-6 3 3 0 000 6z"
                                  clipRule="evenodd"/>
                        </svg>
                        <a className="hover:text-black transition duration-200 ease-linear" href="#">Paramètre</a>
                    </h3>
                </div>
            </div>
            <Script src="https://unpkg.com/@themesberg/flowbite@latest/dist/flowbite.bundle.js"  strategy="beforeInteractive"/>
            </>
    )
}
