
import ButtonNext from "./buttonNext";
import ButtonPrevious from "./previousButton";
import Link from "next/link";
import Layout from "../../layouts/layout";

export default function Post({ id, title, body, date, page}) {
    return (
        <Layout>
                <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                    <div className="lg:text-center mt-10">
                        <p className="mt-2 text-xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">

                            <Link href={`/blog_post/${id}`}>
                                <a>
                                    <h1>{title}</h1>
                                </a>
                            </Link>
                        </p>
                    </div>
                    <div className="m-10">
                        <dl className="ml-16 mt-16 items-center space-y-10 md:space-y-0 md:grid-cols-2 md:gap-x-8 md:gap-y-10">
                            <div className="relative">
                                <dt>
                                    <div
                                        className="absolute flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
                                        <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none"
                                             viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                                  d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z"/>
                                        </svg>
                                    </div>
                                    <p className="ml-16 text-lg leading-6 font-medium text-gray-900">
                                        Publication : {id}</p>
                                </dt>
                                <dd className="mt-2 ml-16 text-base text-gray-500">
                                    {body}
                                </dd>
                            </div>
                        </dl>
                    </div>
                    <div className="lg:text-right">
                        <p className="mt-4 max-w-2xl text-l text-gray-500 lg:mx-auto">
                            Date : {date}
                        </p>
                    </div>
                </div>
        </Layout>
    )
}