import Post_blog from "../components/post";
import {categories} from "../api/categories";
import {data} from "../api/register";
import Link from "next/link";
import Nav from "../components/nav";
import Footer from "../components/footer";
import Layout from "../../layouts/layout";

export default function Category({cat, post, date, page}) {

    return (
        <Layout>
            <div className="flex flex-col h-screen">
                <div className="flex flex-row">
                    <Nav/>
                    <div className="flex flex-col ml-10">
                        <div className="lg:text-center">
                            <p className="mt-2 text-xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl  p-10  ">
                                {cat.map(data =>
                                    <h1>{data.name}</h1>
                                )}
                            </p>
                        </div>
                        {post.map(data =>
                            <div className=" relative max-w-7xl mx-auto bg-gray-200 m-10 pt-10 pb-10 shadow-2xl">
                                <div className="flex flex-col ml-10">
                                <Post_blog
                                    id={data.id}
                                    title={data.title}
                                    body={data.body}
                                    date = {date}
                                    page = {page}
                                />
                                </div>
                            </div>
                        )}
                    </div>
                </div>
                <Footer/>
            </div>
        </Layout>
    )
}

export async function getStaticProps({params}) {

    const post = []
    const cat = []

    data.forEach(function(dat){
        if (dat.idCategory === parseInt(params.id)){
            post.push(dat);
        }
    });

    categories.forEach(function(elem){
        if (elem.id === parseInt(params.id)){
            cat.push(elem);
        }
    });

    return {
        props: {
            cat,
            post,
            date: (new Date()).toLocaleDateString(),
            page: (parseInt(params.id)),
        }
    }
}

export async function getStaticPaths() {
    return {

        paths : categories.map(category => ({
            params: {id: category.id.toString()}
        })),
        fallback: false,
    }
}