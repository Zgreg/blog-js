export const categories =
    [
        {
            "id": 1,
            "name": "Développement web",
        },
        {
            "id": 2,
            "name": "Anglais",
        },
        {
            "id": 3,
            "name": "Développement 3D",
        },
        {
            "id": 4,
            "name": "Sécurisation de code",
        }
    ]