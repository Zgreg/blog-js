import {NextApiRequest, NextApiResponse} from "next";
import jwt from 'jsonwebtoken'

const KEY = 'dfjskfdikdsokqskodpqkdqo'

export default function (req: NextApiRequest, res: NextApiResponse){
    const {token} = req.body
    const {admin} = jwt.verify(token, KEY)

    if (admin){
        res.json({ secretAdminCode: token})
    } else {
        res.json({ admin: false})
    }
}