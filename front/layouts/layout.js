import React, {useEffect} from 'react';
import Head from "next/head";
import Link from "next/link";
import Router, {useRouter} from "next/router";

const Layout = (props) => {
    const router = useRouter();

    useEffect(() => {
        let token
        token = localStorage.getItem('token')
        if (token === null) {
            Router.push('/')
        }
    }, [])

    return (
        <>
            <main className="form-signin">
                {props.children}
            </main>
        </>
    );
};

export default Layout;
