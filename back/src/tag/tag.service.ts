import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Tag } from './interfaces/tag.interface';
import { CreateTagDTO } from './dto/create-tag.dto';

@Injectable()
export class TagService {
    constructor(@InjectModel('Tag') private tagModel: Model<Tag>) {}

    async addTag(createTagDTO: CreateTagDTO): Promise<Tag> {
        const newTag = await new this.tagModel(createTagDTO);
        return newTag.save();
    }

    async getTags(): Promise<Tag[]> {
        const tags = await this.tagModel.find().exec();
        return tags;
    }

    async getTag(tagID): Promise<Tag> {
        const tag = await this.tagModel.findById(tagID).exec();
        return tag;
    }

    async editTag(tagID, createTagDTO: CreateTagDTO): Promise<Tag> {
        const editedTag = await this.tagModel.findByIdAndUpdate(tagID, createTagDTO, { new: true });
        return editedTag;
    }

    async deleteTag(tagID): Promise<any> {
        const deletedTag = await this.tagModel.findByIdAndRemove(tagID);
        return deletedTag;
    }
}
