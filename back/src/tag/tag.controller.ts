import { Controller, Get, Res, HttpStatus, Param, NotFoundException, Post, Body, Query, Put, Delete } from '@nestjs/common';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';
import { TagService } from './tag.service';
import { CreateTagDTO } from './dto/create-tag.dto';

@Controller('tag')
export class TagController {
    constructor(private tagService: TagService) { }

    @Post('/')
    async addTag(@Res() res, @Body() createTagDTO: CreateTagDTO) {
        const newTag = await this.tagService.addTag(createTagDTO);
        return res.status(HttpStatus.OK).json({
            message: "Tag has been submitted successfully!",
            tag: newTag
        })
    }

    @Get('/')
    async getTags(@Res() res) {
        const Tags = await this.tagService.getTags();
        return res.status(HttpStatus.OK).json(Tags);
    }

    @Get('/:tagID')
    async getTag(@Res() res, @Param('tagID', new ValidateObjectId()) tagID) {
        const tag = await this.tagService.getTag(tagID);
        if (!tag) throw new NotFoundException('Tag does not exist!');
        return res.status(HttpStatus.OK).json(tag);
    }

    @Put('/:tagID')
    async editTag(@Res() res, @Param('tagID', new ValidateObjectId()) tagID, @Body() createTagDTO: CreateTagDTO) {
        console.log(tagID);
        
        const editedTag = await this.tagService.editTag(tagID, createTagDTO);
        if (!editedTag) throw new NotFoundException('Tag does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Tag has been successfully updated',
            tag: editedTag
        })
    }

    @Delete('/:tagID')
    async deleteTag(@Res() res, @Param('tagID', new ValidateObjectId()) tagID) {
        const deletedTag = await this.tagService.deleteTag(tagID);
        if (!deletedTag) throw new NotFoundException('Tag does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Tag has been deleted!',
            tag: deletedTag
        })
    }
}
