import { Module } from '@nestjs/common';
import { TagService } from './tag.service';
import { TagController } from './tag.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { TagSchema } from './schemas/tag.schema';

@Module({
  providers: [TagService],
  controllers: [TagController],
  imports: [
      MongooseModule.forFeature([{ name: 'Tag', schema: TagSchema }])
  ],
})
export class TagModule {}
