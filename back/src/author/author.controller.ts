import { Controller, Get, Res, HttpStatus, Param, NotFoundException, Post, Body, Query, Put, Delete } from '@nestjs/common';
import { AuthorService } from './author.service';
import { CreateAuthorDTO } from './dto/create-author.dto';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';

@Controller('author')
export class AuthorController {

    constructor(private authorService: AuthorService) { }

    @Post('/')
    async addAuthor(@Res() res, @Body() createAuthorDTO: CreateAuthorDTO) {
        const newAuthor = await this.authorService.addAuthor(createAuthorDTO);
        return res.status(HttpStatus.OK).json({
            message: "Author has been submitted successfully!",
            post: newAuthor
        })
    }

    @Get('/')
    async getAuthors(@Res() res) {
        const authors = await this.authorService.getAuthors();
        return res.status(HttpStatus.OK).json(authors);
    }

    @Get('/:authorID')
    async getAuthor(@Res() res, @Param('authorID', new ValidateObjectId()) authorID) {
        const author = await this.authorService.getAuthor(authorID);
        if (!author) throw new NotFoundException('author does not exist!');
        return res.status(HttpStatus.OK).json(author);
    }

    @Put('/:authorID')
    async editAuthor(@Res() res, @Param('authorID', new ValidateObjectId()) authorID, @Body() createAuthorDTO: CreateAuthorDTO) {
        console.log(authorID);
        
        const editedAuthor = await this.authorService.editAuthor(authorID, createAuthorDTO);
        if (!editedAuthor) throw new NotFoundException('Author does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Author has been successfully updated',
            author: editedAuthor
        })
    }

    // @Delete('/:authorID')
    // async deleteAuthor(@Res() res, @Param('authorID', new ValidateObjectId()) authorID) {
    //     const deletedAuthor = await this.authorService.deleteAuthor(authorID);
    //     if (!deletedAuthor) throw new NotFoundException('Author does not exist!');
    //     return res.status(HttpStatus.OK).json({
    //         message: 'Author has been deleted!',
    //         author: deletedAuthor
    //     })
    // }
}
