import { Document } from 'mongoose';

export interface Author extends Document {
    readonly complete_name: string;
    readonly email: string;
}