export class CreateAuthorDTO {
    readonly complete_name: string;
    readonly email: string;
}