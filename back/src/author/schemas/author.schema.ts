import * as mongoose from 'mongoose';

export const AuthorSchema = new mongoose.Schema({
    complete_name: String,
    email: String,
})