import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Author } from './interfaces/author.interface';
import { CreateAuthorDTO } from './dto/create-author.dto';


@Injectable()
export class AuthorService {
    constructor(@InjectModel('Author') private authorModel: Model<Author>) {}

    async addAuthor(createAuthorDTO: CreateAuthorDTO): Promise<Author> {
        const newAuthor = await new this.authorModel(createAuthorDTO);
        return newAuthor.save();
    }

    async getAuthors(): Promise<Author[]> {
        const Authors = await this.authorModel.find().exec();
        return Authors;
    }

    async getAuthor(authorID): Promise<Author> {
        const Author = await this.authorModel.findById(authorID).exec();
        return Author;
    }

    async editAuthor(authorID, createAuthorDTO: CreateAuthorDTO): Promise<Author> {
        const editedAuthor = await this.authorModel.findByIdAndUpdate(authorID, createAuthorDTO, { new: true });
        return editedAuthor;
    }

    //TODO Set up a verification: If the author has created a post, it cannot be deleted
    // async deleteAuthor(authorID): Promise<any> {
    //     const deletedAuthor = await this.authorModel.findByIdAndRemove(authorID);
    //     return deletedAuthor;
    // }

}
