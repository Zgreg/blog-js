import { Controller, Get, Res, HttpStatus, Param, NotFoundException, Post, Body, Query, Put, Delete } from '@nestjs/common';
import { CommentService } from './comment.service';
import { CreateCommentDTO } from './dto/create-comment.dto';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';

@Controller('comment')
export class CommentController {
    constructor(private commentService: CommentService) {}

    @Post('/')
    async addPost(@Res() res, @Body() createCommentDTO: CreateCommentDTO) {
        const newComment = await this.commentService.addComment(createCommentDTO);
        return res.status(HttpStatus.OK).json({
            message: "Comment has been submitted successfully!",
            comment: newComment
        })
    }

    @Get('/')
    async getComments(@Res() res) {
        const comments = await this.commentService.getComments();
        return res.status(HttpStatus.OK).json(comments);
    }

    @Get('/post/:postID')
    async getCommentsByPost(@Res() res, @Param('postID', new ValidateObjectId()) postID) {
        const comments = await this.commentService.getCommentsByPost(postID);
        return res.status(HttpStatus.OK).json(comments);
    }

    @Get('/:commentID')
    async getComment(@Res() res, @Param('commentID', new ValidateObjectId()) commentID) {
        const comment = await this.commentService.getComment(commentID);
        if (!comment) throw new NotFoundException('Comment does not exist!');
        return res.status(HttpStatus.OK).json(comment);

    }

    @Put('/:commentID')
    async editComment(@Res() res, @Param('commentID', new ValidateObjectId()) commentID, @Body() createCommentDTO: CreateCommentDTO) {
        const editedComment = await this.commentService.editComment(commentID, createCommentDTO);
        if (!editedComment) throw new NotFoundException('Comment does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Comment has been successfully updated',
            comment: editedComment
        })
    }

    @Put('/:commentID/score/up')
    async incrementScore(@Res() res, @Param('commentID', new ValidateObjectId()) commentID) {
        const editedComment = await this.commentService.changeScore(commentID, 1);
        if (!editedComment) throw new NotFoundException('Comment does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Score has been successfully incremented',
            comment: editedComment
        })
    }

    @Put('/:commentID/score/down')
    async decrementScore(@Res() res, @Param('commentID', new ValidateObjectId()) commentID) {
        const editedComment = await this.commentService.changeScore(commentID, -1);
        if (!editedComment) throw new NotFoundException('Comment does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Score has been successfully decremented',
            comment: editedComment
        })
    }

    @Delete('/:commentID')
    async deleteComment(@Res() res, @Param('postID', new ValidateObjectId()) commentID) {
        const deletedComment = await this.commentService.deleteComment(commentID);
        if (!deletedComment) throw new NotFoundException('Post does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Post has been deleted!',
            comment: deletedComment
        })
    }

}
