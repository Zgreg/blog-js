import { Types } from 'mongoose';

export class CreateCommentDTO {
    readonly author: Types.ObjectId;
    readonly content: string;
    readonly post: Types.ObjectId;
    readonly score: Number;
    readonly date: Date;
}