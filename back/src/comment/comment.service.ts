import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Comment } from './interfaces/comment.interface';
import { CreateCommentDTO } from './dto/create-comment.dto';
import { Author } from '../author/interfaces/author.interface';

@Injectable()
export class CommentService {
    constructor(@InjectModel("Comment") private commentModel: Model<Comment>, @InjectModel("Author") private authorModel: Model<Author>) {}

    async addComment(createCommentDTO: CreateCommentDTO): Promise<Comment> {
        const newComment = await new this.commentModel(createCommentDTO);
        
        return newComment.save();
    }

    async getComments(): Promise<Comment[]> {
        const comments = await this.commentModel.find().populate('author', '', this.authorModel).exec();
        return comments;
    }

    async getCommentsByPost(postID): Promise<Comment[]> {
        const comments = await this.commentModel.find({ 'post' : postID }).populate('author', '', this.authorModel).exec();
        return comments;
    }

    async getComment(commentID): Promise<Comment> {
        const comment = await this.commentModel.findById(commentID).populate('author', '', this.authorModel).exec();
        return comment;
    }

    async editComment(commentID, createCommentDTO: CreateCommentDTO): Promise<Comment> {
        const editedComment = await this.commentModel.findByIdAndUpdate(commentID, createCommentDTO, { new: true });
        return editedComment;
    }

    async changeScore(commentID, value): Promise<Comment> {
        const editedComment = await this.commentModel.findByIdAndUpdate(commentID, { $inc: { score : value } }, { new: true });
        return editedComment;
    }
    
    // async incrementScore(commentID): Promise<Comment> {
    //     const editedComment = await this.commentModel.findByIdAndUpdate(commentID, { $inc: { score : 1 } }, { new: true });
    //     return editedComment;
    // }

    // async decrementScore(commentID): Promise<Comment> {
    //     const editedComment = await this.commentModel.findByIdAndUpdate(commentID, { $inc: { score : -1 } }, { new: true });
    //     return editedComment;
    // }

    async deleteComment(commentID): Promise<any> {
        const deletedComment = await this.commentModel.findByIdAndRemove(commentID);
        return deletedComment;
    }
}
