import { Document, Types } from 'mongoose';

export interface Comment extends Document {
    readonly author: Types.ObjectId;
    readonly content: string;
    readonly post: Types.ObjectId;
    readonly score: Number;
    readonly date: Date;
}