import * as mongoose from 'mongoose';

export const CommentSchema = new mongoose.Schema({
    author: { type : mongoose.Types.ObjectId, ref: 'Author' },
    content: String,
    post : { type : mongoose.Types.ObjectId, ref: 'Post' },
    score: {type : Number, default : 0},
    created_at: { type: Date, default: Date.now }
})