import { Module } from '@nestjs/common';
import { CommentService } from './comment.service';
import { CommentController } from './comment.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { CommentSchema } from './schemas/comment.schema';
import { AuthorSchema } from '../author/schemas/author.schema';

@Module({
  providers: [CommentService],
  controllers: [CommentController],
  imports: [
    MongooseModule.forFeature([
      { name: 'Comment', schema: CommentSchema },
      { name: 'Author', schema: AuthorSchema }
    ])
  ]
})
export class CommentModule {}
