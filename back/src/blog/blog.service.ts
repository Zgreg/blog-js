import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Post } from './interfaces/post.interface';
import { Author } from '../author/interfaces/author.interface';
import { Tag } from '../tag/interfaces/tag.interface';
import { CreatePostDTO } from './dto/create-post.dto';

@Injectable()
export class BlogService {
    constructor(@InjectModel("Post") private postModel: Model<Post>, @InjectModel("Author") private authorModel: Model<Author>, @InjectModel("Tag") private tagModel: Model<Tag>) {}

    async addPost(createPostDTO: CreatePostDTO): Promise<Post> {
        const newPost = await new this.postModel(createPostDTO);
        return newPost.save();
    }

    async getPosts(): Promise<Post[]> {
        const posts = await this.postModel.find().populate('author', '', this.authorModel).populate('tag', '', this.tagModel).exec();
        return posts;
    }

    async getPost(postID): Promise<Post> {
        const post = await this.postModel.findById(postID).populate('author', '', this.authorModel).populate('tag', '', this.tagModel).exec();
        return post;
    }

    async editPost(postID, createPostDTO: CreatePostDTO): Promise<Post> {
        const editedPost = await this.postModel.findByIdAndUpdate(postID, createPostDTO, { new: true });
        return editedPost;
    }

    async deletePost(postID): Promise<any> {
        const deletedPost = await this.postModel.findByIdAndRemove(postID);
        return deletedPost;
    }
}
