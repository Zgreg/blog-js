import * as mongoose from 'mongoose';

export const BlogSchema = new mongoose.Schema({
    title: String,
    body: String,
    // author: { body : Object, by : mongoose.Types.ObjectId},
    author: { type : mongoose.Types.ObjectId, ref: 'Author' },
    tag: { type : mongoose.Types.ObjectId, ref: 'Tag' },
    image: String,
    created_at: { type: Date, default: Date.now }
})