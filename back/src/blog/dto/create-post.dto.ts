import { Types } from 'mongoose';

export class CreatePostDTO {
    readonly title: string;
    readonly description: string;
    readonly body: string;
    readonly author: Types.ObjectId;
    readonly tag: Types.ObjectId;
    readonly date_posted: string
}