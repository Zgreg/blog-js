import { Module } from '@nestjs/common';
import { BlogService } from './blog.service';
import { MongooseModule } from '@nestjs/mongoose';
import { BlogSchema } from './schemas/blog.schema';
import { AuthorSchema } from '../author/schemas/author.schema';
import { TagSchema } from '../tag/schemas/tag.schema';
import { BlogController } from './blog.controller';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: 'Post', schema: BlogSchema },
            { name: 'Author', schema: AuthorSchema },
            { name: 'Tag', schema: TagSchema }
        ])
    ],
    providers: [BlogService],
    controllers: [BlogController]
})
export class BlogModule {}
