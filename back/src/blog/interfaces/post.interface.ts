import { Document, Types } from 'mongoose';

export interface Post extends Document {
    readonly title: string;
    readonly description: string;
    readonly body: string;
    readonly author: Types.ObjectId;
    readonly tag: Types.ObjectId;
    readonly date_posted: string
}